package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;
	
	PolylineOptions po = new PolylineOptions();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		
		po.width(5);
		//po.add(new LatLng(0,0));

		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				currentLocation = l;
				drawLine(currentLocation.getLatitude(), currentLocation.getLongitude());
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		// Update location every 5 seconds or moved more than 5 meters > call onLocationChanged
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}
	
	private void drawLine(double latitude, double longitude) {
		// TODO Auto-generated method stub
		po.add(new LatLng(latitude,longitude));	
		map.clear();
		map.addPolyline(po);
//		MarkerOptions mo = new MarkerOptions();
//		mo.position(new LatLng(latitude,longitude));
//		map.addMarker(mo);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId(); 
		switch(id) {
		case R.id.action_mark:
			locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
			currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			MarkerOptions mo = new MarkerOptions();
			
			mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
			mo.title("You");
			map.addMarker(mo);
			return true;
		case R.id.action_changecolor:
			Random rnd = new Random(); 
			int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)); 
			map.clear();
			map.addPolyline(po.color(color));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		} 
	}
}
